from setuptools import setup, find_packages

with open("requirements.txt") as f:
	install_requires = f.read().strip().split("\n")

# get version from __version__ variable in syngene_planning/__init__.py
from syngene_planning import __version__ as version

setup(
	name="syngene_planning",
	version=version,
	description="Test Sample Planning App",
	author="Castlecraft Ecommerce Pvt. Ltd.",
	author_email="support@castlecraft.in",
	packages=find_packages(),
	zip_safe=False,
	include_package_data=True,
	install_requires=install_requires
)
