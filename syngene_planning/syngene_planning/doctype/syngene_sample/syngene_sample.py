# Copyright (c) 2021, Castlecraft Ecommerce Pvt. Ltd. and contributors
# For license information, please see license.txt

import frappe
import uuid

from frappe.model.document import Document

class SyngeneSample(Document):
	def autoname(self):
		self.name = str(uuid.uuid4())

	def validate(self):
		self.submitted_by_name = frappe.get_value("Syngene Employee", self.submitted_by, "employee_name")
